import numpy as np
import os

files = ['vmd_gAg_u-O_v.dat', 'vmd_gAg_u-H_v.dat']
times = [19] + [19 * i for i in range(1, 8)]
odirs = [f'{time:03d}ns' for time in times[2:]]
for odir in odirs:
    os.makedirs(odir, exist_ok=True)

indirs = [('Ag+_12-6_prod_02', 'Ag+_12-6_prod_03'),  # -> 38ns
          ('Ag+_12-6_prod_04', '038ns'),  # -> 57
          ('Ag+_12-6_prod_05', '057ns'),  # -> 76
          ('Ag+_12-6_prod_06', '076ns'),  # ->  ... 
          ('Ag+_12-6_prod_07', '095ns'),
          ('Ag+_12-6_prod_08', '114ns')]

for fn in files:
    print(fn)
    for i, (odir, dirs) in enumerate(zip(odirs, indirs)):
        dat1 = np.genfromtxt(dirs[0] + '/' + fn)
        dat2 = np.genfromtxt(dirs[1] + '/' + fn)

        t1 = times[i] 
        t2 = times[i + 1]
        dat = (dat1[:, 1] * t1 + dat2[:, 1] * t2) / (t1 + t2)
        out = np.vstack((dat1[:, 0], dat)).T
        np.savetxt(odir + '/' + fn, out)
        print(t1,t2, odir)



