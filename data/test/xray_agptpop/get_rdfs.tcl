package require pbctools 2.5

set sels1 [list "name AG" "name PT" "name \"O.\*\" and index < 39" "name \"H.\*\" and index < 39" "name \"P.\*\" and index < 39"]
set sels2 [list "name AG" "name PT" "name \"O.\*\" and index < 39" "name \"H.\*\" and index < 39" "name \"P.\*\" and index < 39"]

set len1 [llength $sels1]
set len2 [llength $sels2]

set names1 [list "Ag_u" "Pt_u" "O_u" "H_u" "P_u"]
set names2 [list "Ag_u" "Pt_u" "O_u" "H_u" "P_u"]

proc rdf {atm1 atm2 outname} {
    set outfile1 [open $outname.dat w]

    set sel1 [atomselect top $atm1]
    set sel2 [atomselect top $atm2]


    pbc set {25 25 25 } -all
    molinfo top set alpha 90
    molinfo top set beta 90
    molinfo top set gamma 90
    set gr0 [measure gofr $sel1 $sel2 delta 0.0001 rmax 12.5 usepbc 1 selupdate 0 first 0 last -1 step 1]
    set r [lindex $gr0 0]
    set gr [lindex $gr0 1]
    set igr [lindex $gr0 2]
    set isto [lindex $gr0 3]
    foreach j $r k $gr l $igr m $isto {
        puts $outfile1 [format "%.4f\t%.4f\t%.4f\t%.4f" $j $k $l $m]
    }
    close $outfile1
}

mol load parm7 agptpop_dry.prmtop xyz agptpop_dry.xyz

for {set ii 0} {$ii < $len1} {incr ii} {
    for {set jj 0} {$jj < $len2} {incr jj} {
    # Only 1 silver:
    if { $ii==0 && $jj==0 } {
	continue 
     }

    set sel1 [lindex $sels1 $ii]
    set sel2 [lindex $sels2 $jj]

    set name g[lindex $names1 $ii]-[lindex $names2 $jj]

    puts $name

    rdf $sel1 $sel2 $name
    }
}
mol delete top

