Finite-size Effects on the Calculation of X-ray Scattering from Molecular Dynamics Simulations
==============================================================================================

Data repository. All plots for the manuscript can be found in `plots.ipynb`

Uses the accompanying [grsq](https://gitlab.com/asod/grsq) module. `pip install grsq`
