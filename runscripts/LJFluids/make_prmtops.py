import numpy as np
from openmmtools.testsystems import LennardJonesFluid
from simtk import unit
import parmed

nparticles = 20713 
sigma_solute = 3.4
sigma = 3.4
box_edge = 100
volume = box_edge**3

ljruns_density = sigma**3 * nparticles / volume
densities = [0.78, 0.80, ljruns_density, 0.82, 0.84]

for reduced_density in densities:
    nparticles = int(np.round(volume * reduced_density / sigma**3))
    fluid = LennardJonesFluid(nparticles=nparticles, sigma=sigma * unit.angstrom,
                              reduced_density=reduced_density)
                              
    tag = f'LJ_{nparticles:d}'
    print(tag, reduced_density)
    struct = parmed.openmm.load_topology(fluid.topology, fluid.system)
    struct.save(f'{tag}.prmtop')
