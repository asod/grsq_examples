Neat water at different densities
--------------------------------- 

The `densities...py` python files are runscripts to simulate the series of water box densities. 

They were originally run on a HPC cluster, the paths have since be changed to all point to this dir, but the files have not been tested after these changes. 

The runscripts use the CMMTools OpenMM wrapper: https://gitlab.com/asod/cmmtools


`rdf_seed.py` Creates tcl scripts for VMD to calculate the RDFs. 
