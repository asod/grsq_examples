import glob
runs = sorted(glob.glob('densities_biggerbox_smallersteps_*_??/'))

for run in runs:
    tag = run.split('/')[-2]    
    with open(tag + '_rdf.tcl', 'w') as f:
        f.write(f'''\
package require pbctools 2.5

# element selector doesnt work on HPC vmd for some odd reason
set sels1 [list  "name O" "name \\"H.*\\""]
set sels2 $sels1 

set len1 [llength $sels1]
set len2 [llength $sels2]

set names1 [list "O_v" "H_v"]
set names2 $names1

### Functions used
proc flist {{ dir ext }} {{
    set contents [glob -directory $dir $ext]
    foreach item $contents {{
       append out $item
       append out "\\n"
       }}
    return $out
    }}


proc rdf {{atm1 atm2 outname dir}} {{
    set outfile1 [open $dir/$outname.dat w]

    set sel1 [atomselect top $atm1]
    set sel2 [atomselect top $atm2]

    molinfo top set alpha 90
    molinfo top set beta 90
    molinfo top set gamma 90
    set gr0 [measure gofr $sel1 $sel2 delta 0.01 rmax 25. usepbc 1 selupdate 0 first 1000 last -1 step 1]; # throw away first 1 ns 
    set r [lindex $gr0 0]
    set gr [lindex $gr0 1]
    set igr [lindex $gr0 2]
    set isto [lindex $gr0 3]
    foreach j $r k $gr l $igr m $isto {{
        puts $outfile1 [format "%.16f\\t%.16f\\t%.16f\\t%.16f" $j $k $l $m]
    }}
    close $outfile1
}}


set dirs [list {run}]

foreach dir $dirs {{
    puts $dir

    set dcds [flist $dir *dcd]
    set dcd [lindex $dcds 0];  # there should only be one file per dir

    mol load parm7 waterbox_4096_amber.prmtop dcd $dcd

    for {{set ii 0}} {{$ii < $len1}} {{incr ii}} {{
        for {{set jj 0}} {{$jj < $len2}} {{incr jj}} {{

            set sel1 [lindex $sels1 $ii]
            set sel2 [lindex $sels2 $jj]

            set name g[lindex $names1 $ii]-[lindex $names2 $jj]

            puts $name

            rdf $sel1 $sel2 $name $dir
            }}
        }}
    mol delete top
}}
''')

