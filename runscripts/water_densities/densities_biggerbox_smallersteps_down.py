from cmm.md.cmmsystems import CMMSystem
import MDAnalysis as MDA
from parmed.openmm.reporters import RestartReporter
from cmm.md.cmmsystems import u, Vec3


''' Increase 0.2 Å per run: 
'''

d = -0.2
for run in range(1, 5):
    # init
    sys = CMMSystem(f'densities_biggerbox_smallersteps_down_{run:02d}', fpath=f'densities_biggerbox_smallersteps_down_{run:02d}/')
    
    # load prmtop (and in this case also inpcrd, which means positions will also be
    # loaded)
    sys.system_from_prmtop('waterbox_4096_amber.prmtop')
    if run == 1:
        sys.load_rst7('water_biggerbox_00.rst7')
    else:
        sys.load_rst7(f'densities_biggerbox_smallersteps_down_{run - 1:02d}/densities_biggerbox_smallersteps_down_{run - 1:02d}.rst7')

    # decrease box
    x, y, z = sys.boxvectors 
    x = x[0]._value
    y = y[1]._value
    z = z[2]._value
    boxvectors = (Vec3((x + d), 0, 0) * u.angstrom,
                  Vec3(0, (y + d), 0) * u.angstrom,
                  Vec3(0, 0, (z + d)) * u.angstrom)
    sys.boxvectors = boxvectors
    print(sys.boxvectors)

    # and integrator
    sys.set_integrator()  

    # init sim
    sys.init_simulation()

    ## minimize
    #sys.minimize()

    # reporters
    sys.standard_reporters()
    restrt = RestartReporter(f'densities_biggerbox_smallersteps_down_{run:02d}/densities_biggerbox_smallersteps_down_{run:02d}.rst7', 1000)
    sys.add_reporter(restrt)

    # runnnn
    sys.run(time_in_ps=10000)
