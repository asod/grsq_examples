import numpy as np
from lmfit import minimize, Parameters
import matplotlib.pyplot as plt

def psi(q, R):
    val = (np.sinc(q * R / np.pi) * (q * R) - q * R * np.cos(q *R)) / (q * R)**3
    val[q == 0] = 1 / 3 # analytic limit
    return val

class ModelFitter:
    ''' Fit analytical expressions of simple geometries to scattering signals '''
    def __init__(self, model='Core-Shell', qmin=1):
        self.model = model
        self.qmin = qmin

        self.target = None
        self.out = None
        self.qvec = None
        self.mdl = None

    def sphere(self, q, R_c, fuzzy=0):
        A = 4 / 3 * np.pi * ( (3 * (np.sin(q * R_c) - R_c * q * np.cos(q * R_c)) / ((q)**3)))
        A *= np.exp(- (fuzzy * q)**2 / 2)
        s = A**2 / (4 / 3 * np.pi * R_c**3)**2
        return s

    def core_shell(self, q, rho_c, rho_s, rho_v, R_c, w_s, scale=1, fuzzy=0):
        R_s = R_c + w_s
        V_c = 4/3 * np.pi * R_c  # not real "volumes" here. Instead, we don't normalize A
        V_s = 4/3 * np.pi * R_s  # with V_s because V_s is less well defined for a solvent shell than the model is made for.
                        # core - shell                         shell - solvent
        A = (3 / V_s) * (V_c * (rho_c - rho_s) * psi(q, R_c) + V_s * (rho_s - rho_v) * psi(q, R_s))
        A *= np.exp(- (fuzzy * q)**2 / 2)
        return scale * A**2 #/ V_s

    def residual(self, params, target, qvec):
        if self.model == 'Core-Shell':
            model = self.core_shell
        elif self.model == 'Sphere':
            model = self.sphere
        else:
            raise RuntimeError('Did not understand what model to fit')
        self.mdl = model

        s = model(qvec[self.qmin:], *[p[1].value for p in params.items()])
        return (s - target[self.qmin:]) / len(qvec[self.qmin:])

    def fit(self, rdfset, parameters, what='DV'):
        if what == 'DV':
            target = rdfset.get_dv()
        elif what == 'solvent':
            target = rdfset.get_solvent()
        elif what == 'cross':
            target = rdfset.get_cross()
        else:
            raise RuntimeError('Did not understand what to fit to')
        qvec = rdfset[0].qvec
        self.qvec = qvec

        self.target = target
        out = minimize(self.residual, parameters, args=(target, qvec))
        self.out = out
        return out

    def plot(self, ax=None, **kwargs):
        model = self.mdl(self.qvec, *[p[1].value for p in self.out.params.items()])
        if ax == None:
            fig, ax = plt.subplots(1, 1, figsize=(9, 4))
        ax.plot(self.qvec, self.target, color='C0', **kwargs)
        ax.plot(self.qvec, model, 'k--', label=self.model)
        ax.plot(self.qvec, self.target- model, 'C3--', label='Residual')
        ax.set_xlabel('q (Å $^{-1}$)')
        ax.set_ylabel('I(q) (a.u.)')
        return ax



